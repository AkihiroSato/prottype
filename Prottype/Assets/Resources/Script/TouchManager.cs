﻿using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour {
    //======================================================================================================
    // 定数
    //======================================================================================================


    //======================================================================================================
    // 変数
    //======================================================================================================
    //--public-----------------------------------
    public GameObject[] m_players;


    //--private----------------------------------
    private int[] m_fingerID;
    private bool[] m_saveFingerID;
    private Vector2[] m_touchPos;
    private Vector2 m_p2actButtonStartPos;
    private GameObject m_touchImage;


    //=======================================================================================================
    // プロパティマネージャ
    //=======================================================================================================
    delegate void SetTouchImageHandle(Vector2 pos);
    SetTouchImageHandle setTouchImageHandle;

    //=======================================================================================================
    // 関数
    //=======================================================================================================
    //--------------------------------------------------------------------------
    // 初期化処理
    void Start()
    {
        m_fingerID = new int[2];
        m_saveFingerID = new bool[10];
        m_touchPos = new Vector2[2];
        for(int i = 0; i < m_fingerID.Length; i++)
        {
            m_fingerID[i] = -1;
        }
        for(int i = 0; i < m_saveFingerID.Length; i++)
        {
            m_saveFingerID[i] = false;
        }

        m_p2actButtonStartPos = new Vector2(Screen.width - 200.0f, Screen.height - 200.0f);

        m_touchImage = Resources.Load("Prefab/TouchImage") as GameObject;
    }


    //--------------------------------------------------------------------------
    // 更新処理
    void Update()
    {
        for (int i = 0; i < 10; i++)
        {
            m_saveFingerID[i] = false;
        }

        int touchCount = Input.touchCount;
        for(int i = 0; i < touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            m_saveFingerID[touch.fingerId] = true;

            if(m_fingerID[0] < 0 && (Screen.width / 2) > touch.position.x)
            {
                GameObject obj = GameObject.Instantiate(m_touchImage) as GameObject;
                setTouchImageHandle = obj.GetComponent<TouchImage>().PlaceImage;
                setTouchImageHandle(touch.position);
                m_fingerID[0] = touch.fingerId;
                m_touchPos[0] = touch.position;
                
            }

            if(m_fingerID[0] == touch.fingerId)
            {
                Vector2 dif = touch.position - m_touchPos[0];
                dif.Normalize();
                m_players[0].SendMessage("SetMoveAxis",dif);
                m_players[0].SendMessage("SetFrontDirect", -Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg);
                setTouchImageHandle(touch.position);
                if(dif.magnitude > 0.001f)
                {
                    m_players[0].SendMessage("SendRun");
                }
                else
                {
                    m_players[0].SendMessage("SendWait");
                }

            }

            if(m_fingerID[1] < 0 && (Screen.width / 2) < touch.position.x)
            {
                m_fingerID[1] = touch.fingerId;
                m_touchPos[1] = touch.position;
            }

            if(m_fingerID[1] == touch.fingerId)
            {
                Vector2 dif = touch.position - m_touchPos[1];
                dif.Normalize();
                m_players[1].SendMessage("SetMoveAxis",dif);
            }

            if (Utility.CheckRange(touch.position, Vector2.zero, 200.0f, 200.0f))
            {
                m_players[0].SendMessage("SetAura");
            }

            if (Utility.CheckRange(touch.position, m_p2actButtonStartPos, 200.0f, 200.0f))
            {
                m_players[1].SendMessage("SetAura");
            }
        }

        for(int i = 0; i < m_fingerID.Length; i++)
        {
            if(m_fingerID[i] != -1 && m_saveFingerID[m_fingerID[i]] == false)
            {
                m_fingerID[i] = -1;
                m_players[i].SendMessage("SetMoveAxis", Vector2.zero);
                m_players[i].SendMessage("SendWait");
            }
        }
    }
}