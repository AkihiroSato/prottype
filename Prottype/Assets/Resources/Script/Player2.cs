﻿using UnityEngine;
using System.Collections;

public class Player2 : MonoBehaviour {
//======================================================================================================
// 定数
//======================================================================================================
	
	
//======================================================================================================
// 変数
//======================================================================================================
    //--public-----------------------------------
    public float m_moveSpeed = 0.2f;
    public float m_hp = 100;

    //--private----------------------------------
    private GameObject m_aura;
    private bool m_isAuraFlag;
    private Vector2 m_moveAxis;
    private Animator m_animator;
	
	
//=======================================================================================================
// プロパティマネージャ
//=======================================================================================================
    public bool IsAuraFlag
    {
        get { return m_isAuraFlag; }
    }

    public int Hp
    {
        get
        {
            return (int)m_hp;
        }
    }

    public Vector2 MoveAxis
    {
        get
        {
            return m_moveAxis;
        }

        set { m_moveAxis = value; }
    }
	
//=======================================================================================================
// 関数
//=======================================================================================================
	//--------------------------------------------------------------------------
	// 初期化処理
	void Start () 
	{
        m_isAuraFlag = false;

        // オブジェクトを取得
        m_aura = this.transform.FindChild("Aura").gameObject;

        m_animator = GetComponent<Animator>();
	}
	
	
	//--------------------------------------------------------------------------
	// 更新処理
	void Update () 
	{
		// HPを減らす
		m_hp -= Time.deltaTime;
		if (m_hp < 0.0f) 
		{
            m_hp = 0.0f;
			return;
		}

        // 移動処理
        float x = m_moveAxis.y;
        float y = -m_moveAxis.x;
		
		Vector3 thisPos = this.transform.position;
		thisPos.x += x * m_moveSpeed;
		thisPos.z += y * m_moveSpeed;
		
		this.transform.position = thisPos;
	}

    //--------------------------------------------------------------------------
    // Axisを更新
    public void SetMoveAxis(Vector2 axis)
    {
        m_moveAxis = axis;
    }


    //--------------------------------------------------------------------------
    // オーラを起動
    public void SetAura()
    {
        m_isAuraFlag = m_isAuraFlag ? false : true;
        m_aura.renderer.enabled = m_aura.renderer.enabled ? false : true;

    }

    //--------------------------------------------------------------------------
    // ウェイトモーションへ移行
    public void SendWait()
    {
        m_animator.SetBool("run", false);
    }

    //--------------------------------------------------------------------------
    // 走りモーションへ移行
    public void SendRun()
    {
        m_animator.SetBool("run", true);
    }

    //--------------------------------------------------------------------------
    // プレイヤーの向きを設定
    public void SetFrontDirect(float rotate)
    {
        gameObject.transform.rotation = Quaternion.AngleAxis(rotate, Vector3.up);
    }
}